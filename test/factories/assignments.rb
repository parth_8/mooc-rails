FactoryBot.define do
  factory :assignment do
    title { Faker::Name.name }
    content { Faker::Name.name }
    content_media nil
    association :section
  end
end
