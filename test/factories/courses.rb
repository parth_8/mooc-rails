FactoryBot.define do
  factory :course do
    title { Faker::Name.name }
    description { Faker::Name.name }
    status { :drafted }
    price { Random.rand(100..500) }
    created_by_id 0
    user_id 0
    entrollment_allowed_till { 1.hours.after }
    after(:create) do |record, evaluator|
      create_list(:tag, 1)
    end
  end
end
