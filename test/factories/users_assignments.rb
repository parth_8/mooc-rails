FactoryBot.define do
  factory :users_assignment do
    association :user
    association :assignment
    association :user_course
    submission { File.open("spec/testfile.txt", "rb") }
  end
end
