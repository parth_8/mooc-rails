FactoryBot.define do
  factory :section do
    association :course
    title { Faker::Name.name }
    duration_type { 'weekly' }
  end
end
