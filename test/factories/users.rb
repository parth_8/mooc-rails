FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    account_identifier { Faker::App.name.downcase }
    password { 'Password123' }
    role { :student }
  end
end
