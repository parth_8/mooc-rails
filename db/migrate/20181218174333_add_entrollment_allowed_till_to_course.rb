class AddEntrollmentAllowedTillToCourse < ActiveRecord::Migration[5.1]
  def change
    add_column :courses, :entrollment_allowed_till, :datetime
  end
end
