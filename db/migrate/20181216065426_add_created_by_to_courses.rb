class AddCreatedByToCourses < ActiveRecord::Migration[5.1]
  def change
    add_reference :courses, :created_by, foreign_key: { to_table: :users }
  end
end
