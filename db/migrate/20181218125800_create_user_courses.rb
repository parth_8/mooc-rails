class CreateUserCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :user_courses do |t|
      t.references :course, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :price, default: 0
      t.integer :lessons_completed, default: 0
      t.integer :status, default: 0

      t.timestamps
    end

    add_index :user_courses, [:course_id, :user_id], unique: true
  end
end
