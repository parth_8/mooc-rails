class RemoveIndexesFromLessons < ActiveRecord::Migration[5.1]
  def change
    remove_index :lessons, [:sequential_id, :course_id]
  end
end
