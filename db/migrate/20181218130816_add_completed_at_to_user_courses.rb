class AddCompletedAtToUserCourses < ActiveRecord::Migration[5.1]
  def change
    add_column :user_courses, :completed_at, :datetime
  end
end
