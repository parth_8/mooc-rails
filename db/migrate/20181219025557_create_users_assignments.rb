class CreateUsersAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :users_assignments do |t|
      t.references :user, foreign_key: true
      t.references :assignment, foreign_key: true
      t.attachment :submission

      t.timestamps
    end
  end
end
