class AddUserCourseToUsersAssignments < ActiveRecord::Migration[5.1]
  def change
    add_reference :users_assignments, :user_course, foreign_key: true
  end
end
