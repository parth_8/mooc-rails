class CreateCompletionCertificates < ActiveRecord::Migration[5.1]
  def change
    create_table :completion_certificates do |t|
      t.string :reference_number, unique: true
      t.references :user_course, foreign_key: true
      t.references :student, foreign_key: { to_table: :users }
      t.integer :user_id, index: true

      t.timestamps
    end
  end
end
