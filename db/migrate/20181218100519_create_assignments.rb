class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.string :title
      t.text :content
      t.attachment :content_media
      t.references :section
      t.integer :user_id, index: true 

      t.timestamps
    end
  end
end
