class CreateLessons < ActiveRecord::Migration[5.1]
  def change
    create_table :lessons do |t|
      t.references :course, foreign_key: true
      t.integer :sequential_id, index: true, null: false
      t.string :title
      t.text :content
      t.attachment :media

      t.timestamps
    end

    add_index :lessons, [:sequential_id, :course_id], unique: true
  end
end
