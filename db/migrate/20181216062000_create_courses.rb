class CreateCourses < ActiveRecord::Migration[5.1]
  def change
    create_table :courses do |t|
      t.string :title
      t.text :description
      t.integer :price, default: 0
      t.integer :status, default: 0
      t.jsonb :metadata, default: {}

      t.timestamps
    end
  end
end
