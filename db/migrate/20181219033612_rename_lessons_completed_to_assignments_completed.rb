class RenameLessonsCompletedToAssignmentsCompleted < ActiveRecord::Migration[5.1]
  def change
    rename_column :user_courses, :lessons_completed, :assignments_completed
  end
end
