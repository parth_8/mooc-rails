class CreateSections < ActiveRecord::Migration[5.1]
  def change
    create_table :sections do |t|
      t.string :title
      t.references :course
      t.integer :duration_type, default: 0
      t.integer :sequential_id
      t.integer :lessons_count, default: 0

      t.timestamps
    end
  end
end
