class AddDocumentToCompletionCertificates < ActiveRecord::Migration[5.1]
  def up
    add_attachment :completion_certificates, :document
  end

  def down
    remove_attachment :completion_certificates, :document
  end
end
