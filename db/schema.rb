# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181219085020) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.string "content_media_file_name"
    t.string "content_media_content_type"
    t.integer "content_media_file_size"
    t.datetime "content_media_updated_at"
    t.bigint "section_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["section_id"], name: "index_assignments_on_section_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "completion_certificates", force: :cascade do |t|
    t.string "reference_number"
    t.bigint "user_course_id"
    t.bigint "student_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
    t.index ["student_id"], name: "index_completion_certificates_on_student_id"
    t.index ["user_course_id"], name: "index_completion_certificates_on_user_course_id"
    t.index ["user_id"], name: "index_completion_certificates_on_user_id"
  end

  create_table "course_tags", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_course_tags_on_course_id"
    t.index ["tag_id"], name: "index_course_tags_on_tag_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "price", default: 0
    t.integer "status", default: 0
    t.jsonb "metadata", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "created_by_id"
    t.integer "user_id"
    t.integer "lessons_count", default: 0
    t.datetime "entrollment_allowed_till"
    t.index ["created_by_id"], name: "index_courses_on_created_by_id"
    t.index ["user_id"], name: "index_courses_on_user_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.bigint "course_id"
    t.integer "sequential_id", null: false
    t.string "title"
    t.text "content"
    t.string "media_file_name"
    t.string "media_content_type"
    t.integer "media_file_size"
    t.datetime "media_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "section_id"
    t.index ["course_id"], name: "index_lessons_on_course_id"
    t.index ["section_id"], name: "index_lessons_on_section_id"
    t.index ["sequential_id"], name: "index_lessons_on_sequential_id"
  end

  create_table "sections", force: :cascade do |t|
    t.string "title"
    t.bigint "course_id"
    t.integer "duration_type", default: 0
    t.integer "sequential_id"
    t.integer "lessons_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_sections_on_course_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_courses", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "user_id"
    t.integer "price", default: 0
    t.integer "assignments_completed", default: 0
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "completed_at"
    t.index ["course_id", "user_id"], name: "index_user_courses_on_course_id_and_user_id", unique: true
    t.index ["course_id"], name: "index_user_courses_on_course_id"
    t.index ["user_id"], name: "index_user_courses_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "account_identifier"
    t.string "password_digest"
    t.integer "role", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users_assignments", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "assignment_id"
    t.string "submission_file_name"
    t.string "submission_content_type"
    t.integer "submission_file_size"
    t.datetime "submission_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_course_id"
    t.index ["assignment_id"], name: "index_users_assignments_on_assignment_id"
    t.index ["user_course_id"], name: "index_users_assignments_on_user_course_id"
    t.index ["user_id"], name: "index_users_assignments_on_user_id"
  end

  add_foreign_key "completion_certificates", "user_courses"
  add_foreign_key "completion_certificates", "users", column: "student_id"
  add_foreign_key "course_tags", "courses"
  add_foreign_key "course_tags", "tags"
  add_foreign_key "courses", "users", column: "created_by_id"
  add_foreign_key "lessons", "courses"
  add_foreign_key "lessons", "sections"
  add_foreign_key "user_courses", "courses"
  add_foreign_key "user_courses", "users"
  add_foreign_key "users_assignments", "assignments"
  add_foreign_key "users_assignments", "user_courses"
  add_foreign_key "users_assignments", "users"
end
