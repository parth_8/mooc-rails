FROM parthmodi/rails_base:latest

RUN apt-get update && \
    apt-get install -y libpq-dev imagemagick && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /home/app
WORKDIR /home/app

COPY Gemfile* /home/app/
COPY docker /home/app/docker/
RUN bundle install

# Create user and group
RUN groupadd --gid 9999 app && \
    useradd --uid 9999 --gid app app && \
    chown -R app:app /home/app

# Add the Rails app
COPY . /home/app

# WORKDIR /home/app

# RUN chmod +x docker/entrypoint.sh
ENV APPNAME mooc_rails

EXPOSE 3000

ENTRYPOINT [ "docker/entrypoint.sh" ]
CMD [ "bundle", "exec", "rails", "server", "-p", "3000" ]
