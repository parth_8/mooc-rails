# frozen_string_literal: true

class Auth::UnauthorizedAccessError < StandardError; end
