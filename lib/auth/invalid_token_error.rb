# frozen_string_literal: true

class Auth::InvalidTokenError < StandardError; end
