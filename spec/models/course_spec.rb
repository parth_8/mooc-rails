require 'rails_helper'

RSpec.describe Course, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(1).is_at_most(120) }

    it { is_expected.to validate_presence_of(:description) }
    
    it { is_expected.to validate_presence_of(:price) }
    it { is_expected.to validate_numericality_of(:price).only_integer.is_greater_than_or_equal_to(0) }

    it { is_expected.to validate_presence_of(:status) }
    
    context 'entrollment_allowed_till' do
      before do
        @user = FactoryBot.create(:user)
        @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      end
      subject { FactoryBot.build(:course, created_by_id: @user.id, user_id: @user.id) }
      context 'when course is not published' do
        it 'entrollment_allowed_till is not required' do
          expect(subject.valid?).to eq(true)
        end
      end
    end
  end

  describe '.filter_by_status(input_status)' do
    before(:all) do
      @user = FactoryBot.create(:user)
      @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @course2 = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @course3 = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @course.update(status: :drafted)
      @course2.update(status: :published)
      @course3.update(status: :archived)
    end

    after(:all) do
      Course.destroy_all
    end
    
    context 'when records present' do
      context 'when nil passed' do
        it 'returns empty association' do
          expect(Course.filter_by_status(nil).to_a).to eq([])
        end
      end

      context 'when drafted passed' do
        it 'returns drafted records' do
          
          expect(Course.filter_by_status('drafted').ids).to eq([@course.id])
        end
      end

      context 'when published passed' do
        it 'returns published records' do
          expect(Course.filter_by_status('published').ids).to eq([@course2.id])
        end
      end

      context 'when archived passed' do
        it 'returns archived records' do
          expect(Course.filter_by_status('archived').ids).to eq([@course3.id])
        end
      end

      context 'when all passed' do
        it 'returns all records' do
          expect(Course.filter_by_status('all').ids.sort).to eq([@course.id, @course2.id, @course3.id].sort)
        end
      end
    end
  end

  describe '.filter_by_price(min, max)' do
    before(:all) do
      @user = FactoryBot.create(:user)
      @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @course2 = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @course2.update(price: @course.price + 100)
      @min = @course.price - 10
      @max = @course.price + 10
    end

    after(:all) do
      Course.destroy_all
    end
    
    context 'when records present' do
      context 'when nil passed for min and max' do
        it 'returns empty association' do
          expect(Course.filter_by_price(nil, nil).to_a).to eq([])
        end
      end

      context 'when max is smaller than min' do
        it 'returns empty association' do
          expect(Course.filter_by_price(@max, @min).to_a).to eq([])
        end
      end

      context 'when min and max passed' do
        it 'returns records falling within range' do
          expect(Course.filter_by_price(@min, @max).ids).to eq([@course.id])
        end
      end
    end
  end
end
 