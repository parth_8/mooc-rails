require 'rails_helper'

RSpec.describe User, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }

    context 'for course_creator' do
      subject { User.new(role: :course_creator) }
      it { is_expected.to validate_presence_of(:account_identifier) }
      it { is_expected.to validate_uniqueness_of(:account_identifier).case_insensitive }
      it { is_expected.to validate_length_of(:account_identifier).is_at_least(3).is_at_most(6) }
    end

    it { is_expected.to validate_presence_of(:password) }
    it { is_expected.to validate_presence_of(:role) }
  end

  context 'before_validate callbacks' do
    context 'sanitize_email' do
      context 'when email is in uppercase' do
        it 'updates assigned email to lowercase' do
          user = User.new(email: "RANDOM@GMAIL.COM")
          user.valid?
          expect(user.email).to eq('RANDOM@GMAIL.COM'.downcase)
        end
      end 
      context 'when email has extra whitespace' do
        it 'updates trips whitespace from assigned email' do
          user = User.new(email: "abc@gmail.com   ")
          user.valid?
          expect(user.email).to eq('abc@gmail.com')
        end
      end
    end

    context 'sanitize_account_identifier' do
      context 'when account_identifier is in uppercase' do
        it 'updates assigned account_identifier to lowercase' do
          user = User.new(account_identifier: "ABCD")
          user.valid?
          expect(user.account_identifier).to eq('ABCD'.downcase)
        end
      end 
      context 'when account_identifier has extra whitespace' do
        it 'updates trips whitespace from assigned email' do
          user = User.new(account_identifier: "abcD   ")
          user.valid?
          expect(user.account_identifier).to eq('abcd')
        end
      end
    end
  end
end
