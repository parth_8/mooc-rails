require 'rails_helper'

RSpec.describe Lesson, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(1).is_at_most(120) }
  end
end
