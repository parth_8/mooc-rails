require 'rails_helper'

RSpec.describe UserCourse, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:course) }
    it { is_expected.to belong_to(:user) }
  end

  context 'validations' do
    before do
      @user = FactoryBot.create(:user)
      @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
    end
    subject { UserCourse.new(user_id: @user.id, course_id: @course.id) }
    it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:course_id) }
    it { is_expected.to validate_numericality_of(:price).only_integer.is_greater_than_or_equal_to(0) }
  end

  context 'callbacks' do
    before(:all) do
      @user = FactoryBot.create(:user)
      @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
    end

    describe 'before_save :mark_as_completed' do
      subject { FactoryBot.build(:user_course, user: @user, course: @course) }
      
      context 'when completed_at is nil while saving' do
        it 'does not update status field' do
          subject.completed_at = nil
          subject.save
          expect(subject.status).not_to eq(:completed)
        end
      end

      context 'when completed_at is present while saving' do
        it 'marks course as completed' do
          subject.completed_at = Time.zone.now
          subject.save
          expect(subject.status).to eq('completed')
        end
      end
    end

    describe 'after_commit generate_completion_certificate' do
      subject { FactoryBot.build(:user_course, user: @user, course: @course) }
      
      context 'when completed_at is nil while saving' do
        it 'does not generate completion certificate' do
          @count = GenerateCompletionCertificateWorker.jobs.size
          subject.completed_at = nil
          subject.save
          
          expect(GenerateCompletionCertificateWorker.jobs.size).to eq(@count)
        end
      end

      context 'when completed_at is present while saving' do
        it 'enqueues job to generate completion certificate' do
          @count = GenerateCompletionCertificateWorker.jobs.size
          subject.completed_at = Time.zone.now
          subject.save

          expect(GenerateCompletionCertificateWorker.jobs.size).to eq(@count + 1)
        end
      end
    end
  end
end
