require 'rails_helper'

RSpec.describe Section, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:course) }
    it { is_expected.to have_many(:lessons).dependent(:destroy) }
  end

  context 'class methods' do
    describe '.by_role(user_id, role)' do
      before do
        @user1 = FactoryBot.create(:user)
        @user1.update(role: :course_creator)
        @user2 = FactoryBot.create(:user)
        @user1.update(role: :course_creator)

        @course1 = FactoryBot.create(:course, created_by_id: @user1.id, user_id: @user1.id)
        @course2 = FactoryBot.create(:course, created_by_id: @user2.id, user_id: @user2.id)

        @section1 = FactoryBot.create(:section, course: @course1)
        @section2 = FactoryBot.create(:section, course: @course2)
      end
      context 'with course_creator role' do
        it 'returns only the course_creator\'s course sections' do
          expect(Section.by_role(@user1.id, 'course_creator').ids).to eq([@section1.id])
        end
      end

      context 'with student role' do
        it 'returns all course sections' do
          expect(Section.by_role(@user1.id, 'student').ids.sort).to eq([@section1.id, @section2.id].sort)
        end
      end
    end
  end
end
