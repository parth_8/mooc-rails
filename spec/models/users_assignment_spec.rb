require 'rails_helper'

RSpec.describe UsersAssignment, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:assignment) }
    it { is_expected.to belong_to(:user_course) }
  end

  context 'callbacks' do
    before do
      @user = FactoryBot.create(:user)
      ActsAsTenant.current_tenant = @user
      @course = FactoryBot.create(:course, created_by_id: @user.id, user_id: @user.id)
      @section = FactoryBot.create(:section, course: @course)
      @assignment = FactoryBot.create(:assignment, section: @section)
      @user_course = FactoryBot.create(:user_course, user: @user, course: @course)
      @users_assignment = FactoryBot.build(:users_assignment, user: @user, assignment: @assignment, user_course: @user_course)
    end

    describe 'track_course_progress' do
      context 'after create commit' do
        it 'queues TrackCourseProgressWorker' do
          @count = TrackCourseProgressWorker.jobs.size
          @users_assignment.save!
          expect(TrackCourseProgressWorker.jobs.size).to eq(@count + 1)
        end
      end
    end
  end
end
