require 'rails_helper'

RSpec.describe Tag, type: :model do
  describe 'callbacks' do
    context 'sanitize_name' do
      context 'when name is in uppercase' do
        it 'updates assigned name to lowercase' do
          tag = Tag.new(name: "RANDOM")
          tag.valid?
          expect(tag.name).to eq('RANDOM'.downcase)
        end
      end 
      context 'when tag has extra whitespace' do
        it 'updates trips whitespace from assigned tag' do
          tag = Tag.new(name: "tag   ")
          tag.valid?
          expect(tag.name).to eq('tag')
        end
      end
    end
  end

  describe 'class methods' do
    describe 'search' do
      context 'when tags present' do
        context 'with matching term' do
          it 'returns tag' do
            tag = FactoryBot.create(:tag, name: "abcd")
            expect(Tag.search('ab').ids).to eq([tag.id])
          end
        end

        context 'with non matching term' do
          it 'returns empty association' do
            FactoryBot.create(:tag, name: "abcd")
            expect(Tag.search('random').ids).to eq([])
          end
        end
      end
    end
  end
end
