require 'rails_helper'

RSpec.describe Assignment, type: :model do
  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(1).is_at_most(120) }

    it { is_expected.to validate_presence_of(:section_id) }
    it { is_expected.to validate_uniqueness_of(:section_id) }
  end
end
