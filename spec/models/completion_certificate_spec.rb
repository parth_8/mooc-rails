require 'rails_helper'

RSpec.describe CompletionCertificate, type: :model do
  context 'associations' do
    it { is_expected.to belong_to(:user_course) }
    it { is_expected.to belong_to(:student) }
  end

  context 'validations' do
    it { is_expected.to validate_uniqueness_of(:reference_number) }
    
    it { is_expected.to validate_presence_of(:user_id) }
  end

  context 'callbacks' do
    describe 'generate_reference_number' do
      it 'generates random reference number before validation' do
        cert = CompletionCertificate.new
        cert.valid?
        expect(cert.reference_number).not_to eq(nil)
      end
    end
  end
end
