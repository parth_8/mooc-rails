# frozen_string_literal: true

class Resolvers::SignInUser < GraphQL::Function
  argument :auth, !Types::AuthProviderEmailInput

  type do
    name 'SigninPayload'

    field :token, types.String
    field :user, Types::UserType
  end

  def call(_obj, args, _ctx)
    resp =
      LoginUser.new(email: args[:auth][:email], password: args[:auth][:password]).call
    return unless resp.success?

    OpenStruct.new({
                     user: resp.data[:user],
                     token: resp.data[:auth_token].token
                   })
  end
end
