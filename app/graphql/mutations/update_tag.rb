# frozen_string_literal: true

module Mutations
  class UpdateTag < GraphQL::Schema::RelayClassicMutation
    field :tag, Types::TagType, null: true
    field :errors, [String], null: true

    argument :id, ID, required: true
    argument :name, String, required: false
    argument :description, String, required: false

    def resolve(input)
      raise Auth::InvalidTokenError if context[:current_user].blank?

      tag = Tag.find(input[:id])
      tag.assign_attributes(input.slice(:name, :description))
      if tag.save
        {
          tag: tag,
          errors: []
        }
      else
        {
          tag: tag,
          errors: tag.errors.full_messages
        }
      end
    end
  end
end
