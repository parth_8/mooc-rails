# frozen_string_literal: true

class Mutations::CreateLesson < Mutations::BaseMutation
  field :lesson, Types::LessonType, null: true
  field :errors, [String], null: true

  argument :title, String, required: false
  argument :section_id, Int, required: true
  argument :media, String, required: false
  argument :media_file_name, String, required: false
  argument :content, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    sanitize_input(input)
    lesson = Lesson.new(input.slice(:title, :section_id, :media, :media_file_name, :content))
    if lesson.save
      {
        lesson: lesson,
        errors: []
      }
    else
      {
        lesson: nil,
        errors: lesson.errors.full_messages
      }
    end
  end

  private

  def sanitize_input(input)
    input.delete(:section_id) unless Section.by_role(context[:current_user].id, context[:current_user].role).where(id: input[:section_id]).exists?
  end
end
