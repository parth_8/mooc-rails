# frozen_string_literal: true

class Mutations::SubscribeCourse < Mutations::BaseMutation
  field :course, Types::CourseType, null: true
  field :errors, [String], null: false

  argument :course_id, Int, required: true

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?
    raise Auth::UnauthorizedAccessError if not_allowed_to_subscribe?

    user_course =
      UserCourse.find_or_initialize_by(course_id: input[:course_id], user_id: context[:current_user].id)
    return already_subscribed if user_course.persisted?
    return course_not_available unless Course.open_for_enrollment.where(id: input[:course_id]).exists?

    user_course.assign_attributes(price: user_course.course.price)
    if user_course.save
      {
        course: user_course.course,
        errors: []
      }
    else
      {
        course: nil,
        errors: [user_course.errors.full_messages]
      }
    end
  end

  private

  def not_allowed_to_subscribe?
    ActsAsTenant.current_tenant.nil? || context[:current_user].role == 'course_creator'
  end

  def already_subscribed
    {
      course: nil,
      errors: [I18n.t('already_subscribed')]
    }
  end

  def course_not_available
    {
      course: nil,
      errors: [I18n.t('errors.not_found', resource: 'Course')]
    }
  end
end
