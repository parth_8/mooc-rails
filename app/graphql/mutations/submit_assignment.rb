# frozen_string_literal: true

class Mutations::SubmitAssignment < Mutations::BaseMutation
  field :users_assignment, Types::UsersAssignmentType, null: true
  field :errors, [String], null: false

  argument :assignment_id, ID, required: true
  argument :submission_file_name, String, required: true
  argument :submission, String, required: true

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?
    raise Auth::UnauthorizedAccessError unless allowed_to_submit?

    @assignment = Assignment.select(:id, :section_id).find_by(id: input[:assignment_id])
    return assignment_not_found unless assignment_available?

    input[:user_course_id] = user_course.id
    users_assignment = UsersAssignment.find_or_initialize_by(assignment_id: input[:assignment_id], user_id: context[:current_user].id)
    users_assignment.assign_attributes(input.slice(:submission, :submission_file_name, :user_course_id))

    if users_assignment.save
      {
        users_assignment: users_assignment,
        errors: []
      }
    else
      {
        users_assignment: nil,
        errors: [users_assignment.errors.full_messages]
      }
    end
  end

  private

  def allowed_to_submit?
    if ActsAsTenant.current_tenant.nil? || context[:current_user].course_creator?
      false
    else
      true
    end
  end

  def assignment_available?
    @assignment.present? && user_course.present?
  end

  def user_course
    @user_course ||= UserCourse.find_by(user_id: context[:current_user].id, course_id: @assignment.section.course_id)
  end

  def assignment_not_found
    {
      users_assignment: nil,
      errors: [I18n.t('errors.not_found', resource: 'Assignment')]
    }
  end
end
