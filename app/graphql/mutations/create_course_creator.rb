module Mutations
  class CreateCourseCreator < Mutations::BaseMutation
    field :user, Types::UserType, null: true
    field :errors, [String], null: false

    argument :name, String, required: true
    argument :account_identifier, String, required: true
    argument :auth, Types::AuthProviderEmailInput, required: true

    def resolve(input)
      user = User.create(
        name: input[:name],
        account_identifier: input[:account_identifier],
        role: 'course_creator',
        email: input[:auth][:email],
        password: input[:auth][:password]
      )
      if user.save
        {
          user: user,
          errors: []
        }
      else
        {
          user: nil,
          errors: [user.errors.full_messages]
        }
      end
    end
  end
end
