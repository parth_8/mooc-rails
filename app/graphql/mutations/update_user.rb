# frozen_string_literal: true

class Mutations::UpdateUser < Mutations::BaseMutation
  null true

  argument :name, String, required: false
  argument :password, String, required: false

  field :user, Types::UserType, null: true
  field :errors, [String], null: false

  def resolve(input)
    current_user = context[:current_user]
    raise Auth::InvalidTokenError unless current_user.present?

    if current_user.update(input.slice(:name, :password))
      {
        user: current_user,
        errors: [],
      }
    else
      {
        user: nil,
        errors: current_user.errors.full_messages
      }
    end
  end
end
