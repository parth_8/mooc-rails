# frozen_string_literal: true

class Mutations::CreateAssignment < Mutations::BaseMutation
  field :assignment, Types::AssignmentType, null: true
  field :errors, [String], null: false

  argument :section_id, Int, required: true
  argument :title, String, required: true
  argument :content, String, required: false
  argument :content_media, String, required: false
  argument :content_media_file_name, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    assignment = Assignment.new(input.slice(:section_id, :title, :content, :content_media, :content_media_file_name))
    if assignment.save
      {
        assignment: assignment,
        errors: []
      }
    else
      {
        assignment: nil,
        errors: assignment.errors.full_messages
      }
    end
  end
end
