# frozen_string_literal: true

class Mutations::CreateSection < Mutations::BaseMutation
  field :section, Types::SectionType, null: true
  field :errors, [String], null: true

  argument :title, String, required: true
  argument :course_id, ID, required: true
  argument :duration_type, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    input = sanititze_input(input)
    section = Section.new(input.slice(:title, :course_id, :duration_type))
    if section.save
      {
        section: section,
        errors: []
      }
    else
      {
        section: nil,
        errors: section.errors.full_messages
      }
    end
  end

  # private

  def sanititze_input(input)
    input.delete(:course_id) unless context[:current_user].courses.where(id: input[:course_id]).exists?
    input.delete(:duration_type) unless Section.duration_types.key?(input[:duration_type])
    input
  end
end
