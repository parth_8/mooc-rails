# frozen_string_literal: true

class Mutations::UpdateSection < Mutations::BaseMutation
  field :section, Types::SectionType, null: true
  field :errors, [String], null: true

  argument :id, ID, required: true
  argument :title, String, required: true
  argument :duration_type, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    sanititze_input(input)
    section = Section.editable_by_user_id(context[:current_user].id).find(input[:id])
    section.assign_attributes(input.slice(:title, :duration_type))
    if section.save
      {
        section: section,
        errors: []
      }
    else
      {
        section: nil,
        errors: section.errors.full_messages
      }
    end
  end

  # private

  def sanititze_input(input)
    input.delete(:duration_type) unless Section.duration_types.key?(input[:duration_type])
    input
  end
end
