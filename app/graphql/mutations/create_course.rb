# frozen_string_literal: true

module Mutations
  class CreateCourse < Mutations::BaseMutation
    field :course, Types::CourseType, null: true
    field :errors, [String], null: true

    argument :title, String, required: true
    argument :description, String, required: true
    argument :status, String, required: false
    argument :price, Integer, required: false
    argument :tag_ids, [Integer], required: false
    argument :entrollment_allowed_till, String, required: false

    def resolve(input)
      raise Auth::InvalidTokenError if context[:current_user].blank?
      raise Auth::UnauthorizedAccessError if ActsAsTenant.current_tenant.id != context[:current_user].id

      creation_params =
        sanitized_input(input).slice(:title, :description, :status, :price, :tag_ids, :created_by_id, :entrollment_allowed_till)
      course = Course.new(creation_params)
      if course.save
        {
          course: course,
          errors: []
        }
      else
        {
          course: nil,
          errors: course.errors.full_messages
        }
      end
    end

    private

    def sanitized_input(input)
      input[:status] = nil unless Course.statuses.key?(input[:status])
      input[:created_by_id] = ActsAsTenant.current_tenant.id
      input
    end
  end
end
