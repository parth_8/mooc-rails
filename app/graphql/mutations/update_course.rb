# frozen_string_literal: true

module Mutations
  class UpdateCourse < GraphQL::Schema::RelayClassicMutation
    field :course, Types::CourseType, null: true
    field :errors, [String], null: true

    argument :id, ID, required: true
    argument :title, String, required: false
    argument :description, String, required: false
    argument :status, String, required: false
    argument :price, Integer, required: false
    argument :tag_ids, [Integer], required: false
    argument :entrollment_allowed_till, String, required: false

    def resolve(input)
      raise Auth::InvalidTokenError if context[:current_user].blank?
      raise Auth::UnauthorizedAccessError if ActsAsTenant.current_tenant.id != context[:current_user].id

      course = Course.find(input[:id])
      course.assign_attributes(sanitized_input(input).slice(:title, :description, :status, :price, :tag_ids, :entrollment_allowed_till))
      if course.save
        {
          course: course,
          errors: []
        }
      else
        puts course.errors.full_messages.inspect
        {
          course: nil,
          errors: course.errors.full_messages
        }
      end
    end

    private

    def sanitized_input(input)
      input.delete(:status) unless Course.statuses.key?(input[:status])
      input
    end
  end
end
