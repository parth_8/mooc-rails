# frozen_string_literal: true

class Mutations::UpdateLesson < Mutations::BaseMutation
  field :lesson, Types::LessonType, null: true
  field :errors, [String], null: true

  argument :id, ID, required: true
  argument :title, String, required: false
  argument :media, String, required: false
  argument :media_file_name, String, required: false
  argument :content, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    lesson = Lesson.editable_by_user_id(context[:current_user].id).find(input[:id])
    lesson.assign_attributes(input.slice(:title, :media, :media_file_name, :content))
    if lesson.save
      {
        lesson: lesson,
        errors: []
      }
    else
      {
        lesson: nil,
        errors: lesson.errors.full_messages
      }
    end
  end
end
