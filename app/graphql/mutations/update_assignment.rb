# frozen_string_literal: true

class Mutations::UpdateAssignment < Mutations::BaseMutation
  field :assignment, Types::AssignmentType, null: true
  field :errors, [String], null: false

  argument :id, ID, required: true
  argument :title, String, required: false
  argument :content, String, required: false
  argument :content_media, String, required: false
  argument :content_media_file_name, String, required: false

  def resolve(input)
    raise Auth::InvalidTokenError if context[:current_user].blank?

    assignment = Assignment.find(input[:id])
    assignment.assign_attributes(input.slice(:title, :content, :content_media, :content_media_file_name))
    if assignment.save
      {
        assignment: assignment,
        errors: []
      }
    else
      {
        assignment: nil,
        errors: assignment.errors.full_messages
      }
    end
  end
end
