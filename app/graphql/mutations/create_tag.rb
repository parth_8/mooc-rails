# frozen_string_literal: true

module Mutations
  class CreateTag < Mutations::BaseMutation
    field :tag, Types::TagType, null: true
    field :errors, [String], null: true

    argument :name, String, required: true
    argument :description, String, required: false

    def resolve(input)
      raise Auth::InvalidTokenError if context[:current_user].blank?

      tag = Tag.new(input.slice(:name, :description))
      if tag.save
        {
          tag: tag,
          errors: []
        }
      else
        {
          tag: nil,
          errors: tag.errors.full_messages
        }
      end
    end
  end
end
