# frozen_string_literal: true

module Mutations
  class DestroyUser < Mutations::BaseMutation
    null true
    field :user, Types::UserType, null: true
    field :errors, [String], null: true

    argument :user_id, ID, required: false

    def resolve(_input)
      current_user = context[:current_user]
      raise Auth::InvalidTokenError unless current_user.present?

      if current_user.destroy
        {
          user: current_user,
          errors: [],
        }
      else
        {
          user: nil,
          errors: current_user.errors.full_messages
        }
      end
    end
  end
end
