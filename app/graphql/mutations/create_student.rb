class Mutations::CreateStudent < Mutations::BaseMutation
  field :user, Types::UserType, null: true
  field :errors, [String], null: false

  argument :name, String, required: true
  argument :auth, Types::AuthProviderEmailInput, required: true

  def resolve(input)
    user = User.create(
      name: input[:name],
      account_identifier: nil,
      role: 'student',
      email: input[:auth][:email],
      password: input[:auth][:password]
    )
    if user.save
      {
        user: user,
        errors: []
      }
    else
      {
        user: nil,
        errors: [user.errors.full_messages]
      }
    end
  end
end
