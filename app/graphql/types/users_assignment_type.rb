# frozen_string_literal: true

class Types::UsersAssignmentType < Types::BaseObject
  graphql_name 'SubmittedAssignment'

  field :id, ID, null: false
  field :user_id, Int, null: false
  field :assignment_id, Int, null: false
  field :user_course_id, Int, null: false
  field :submission_url, String, null: false
  field :user, Types::UserType, null: false
  field :assignment, Types::AssignmentType, null: false

  def submission_url
    @object.submission.url
  end
end
