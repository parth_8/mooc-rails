# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :createStudent, mutation: Mutations::CreateStudent
    field :createCourseCreator, mutation: Mutations::CreateCourseCreator
    field :submitAssignment, mutation: Mutations::SubmitAssignment
    field :subscribeCourse, mutation: Mutations::SubscribeCourse
    field :updateAssignment, mutation: Mutations::UpdateAssignment
    field :createAssignment, mutation: Mutations::CreateAssignment
    field :updateSection, mutation: Mutations::UpdateSection
    field :createSection, mutation: Mutations::CreateSection
    field :updateLesson, mutation: Mutations::UpdateLesson
    field :createLesson, mutation: Mutations::CreateLesson
    field :updateCourse, mutation: Mutations::UpdateCourse
    field :createCourse, mutation: Mutations::CreateCourse
    field :updateTag, mutation: Mutations::UpdateTag
    field :createTag, mutation: Mutations::CreateTag
    field :destroyUser, mutation: Mutations::DestroyUser
    field :signinUser, function: Resolvers::SignInUser.new
    field :updateUser, mutation: Mutations::UpdateUser
  end
end
