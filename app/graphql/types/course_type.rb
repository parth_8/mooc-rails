# frozen_string_literal: true

class Types::CourseType < Types::BaseObject
  graphql_name 'Course'

  field :id, ID, null: false
  field :title, String, null: false
  field :description, String, null: true
  field :status, String, null: false
  field :price, Integer, null: false
  field :entrollment_allowed_till, String, null: true
  field :created_by, Types::UserType, null: false
  field :tags, [Types::TagType], null: true
  field :lessons, [Types::LessonType], null: true
  field :sections, [Types::SectionType], null: true
  field :students, [Types::UserType], null: true
end
