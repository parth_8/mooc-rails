# frozen_string_literal: true

module Types
  class UserType < Types::BaseObject
    graphql_name 'User'

    field :id, ID, null: false
    field :name, String, null: true
    field :email, String, null: false
    field :account_identifier, String, null: true
    field :role, String, null: false
  end
end
