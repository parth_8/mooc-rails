# frozen_string_literal: true

Types::FilterByPriceInput = GraphQL::InputObjectType.define do
  name 'price_filter_input'

  argument :max, !types.Int
  argument :min, !types.Int
end
