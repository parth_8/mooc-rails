# frozen_string_literal: true

class Types::AssignmentType < Types::BaseObject
  graphql_name 'Assignment'

  field :id, ID, null: false
  field :section_id, Int, null: false
  field :title, String, null: false
  field :content, String, null: true
  field :content_media_url, String, null: true
  field :section, Types::SectionType, null: false

  def content_media_url
    @object.content_media.url
  end
end
