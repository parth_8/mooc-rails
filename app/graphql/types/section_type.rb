# frozen_string_literal: true

class Types::SectionType < Types::BaseObject
  graphql_name 'Section'

  field :id, ID, null: false
  field :sequential_id, Int, null: false
  field :title, String, null: false
  field :duration_type, String, null: false
  field :lessons_count, Int, null: true
  field :course, Types::CourseType, null: true
  field :lessons, [Types::LessonType], null: true
  field :assignment, Types::AssignmentType, null: true
end
