# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :user, UserType, null: true do
      description 'Find a user by ID'
      argument :id, ID, required: true
    end

    def user(id:)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      User.find(id)
    end

    field :tags, [TagType], null: true do
      description 'List Tags'
      argument :limit, Integer, default_value: 20, prepare: ->(limit, _ctx) { [limit, 30].min }, required: false
      argument :term, String, required: false
    end

    def tags(limit:, term: nil)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      Tag.search(term).limit(limit)
    end

    field :courses_connection, CourseType.connection_type, null: true do
      description 'List of courses'
      argument :status, String, default_value: :published, required: false
      argument :tag_names, [String], required: false
      argument :term, String, required: false
      argument :price, Types::FilterByPriceInput, required: false
    end

    def courses_connection(status:, tag_names: nil, term: nil, price: nil)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      results = Course.search(term)
      results = results.by_tag_names(tag_names) if tag_names.present?
      results = results.filter_by_price(price[:min], price[:max]) if price.present?
      results = results.filter_by_status(status)
      results
    end

    field :course_catalogue, CourseType.connection_type, null: true do
      description 'List of active courses across tenants'
      argument :tag_names, [String], required: false
      argument :term, String, required: false
      argument :price, Types::FilterByPriceInput, required: false
    end

    def course_catalogue(tag_names: nil, term: nil, price: nil)
      raise Auth::InvalidTokenError if context[:current_user].nil? || context[:current_user].role == 'course_creator'

      ActsAsTenant.without_tenant do
        results = Course.published.search(term)
        results = results.by_tag_names(tag_names) if tag_names.present?
        results = results.filter_by_price(price[:min], price[:max]) if price.present?
        results
      end
    end

    field :course, CourseType, null: true do
      description 'Find course by Id'
      argument :id, ID, required: true
    end

    def course(id:)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      Course.by_role(context[:current_user].id, context[:current_user].role).find(id)
    end

    field :sections, [SectionType], null: true do
      description 'List of sections of a course'
      argument :course_id, ID, required: true
    end

    def sections(course_id:)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      Section.where(course_id: course_id)
    end

    field :section, SectionType, null: true do
      description 'Find Section by Id'
      argument :id, ID, required: true
    end

    def section(id:)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      Section.by_role(context[:current_user].id, context[:current_user].role).find(id)
    end

    field :assignment, AssignmentType, null: true do
      description 'Find Assignment by Id'
      argument :id, ID, required: true
    end

    def assignment(id:)
      raise Auth::InvalidTokenError if context[:current_user].nil?

      Assignment.find(id)
    end

    field :students, UserType.connection_type, null: true do
      description 'Find students entrolled in courses'
      argument :course_ids, [ID], required: false
    end

    def students(course_ids: [])
      raise Auth::InvalidTokenError if context[:current_user].nil?
      raise Auth::UnauthorizedAccessError if ActsAsTenant.current_tenant.nil? || context[:current_user].id != ActsAsTenant.current_tenant.id

      courses =
        if course_ids.blank?
          Course.all
        else
          Course.where(id: course_ids)
        end
      User.by_course_ids(courses)
    end
  end
end
