# frozen_string_literal: true

class Types::LessonType < Types::BaseObject
  graphql_name 'Lesson'

  field :id, ID, null: false
  field :sequential_id, Int, null: false
  field :section_id, Int, null: false
  field :course_id, Int, null: false
  field :title, String, null: false
  field :content, String, null: true
  field :media_url, String, null: true
  field :course, Types::CourseType, null: false
  field :section, Types::SectionType, null: false

  def media_url
    @object.media.url
  end
end
