# frozen_string_literal: true

class Types::TagType < Types::BaseObject
  graphql_name 'Tag'

  field :id, ID, null: false
  field :name, String, null: false
  field :description, String, null: true
  field :courses, [Types::CourseType], null: true
end
