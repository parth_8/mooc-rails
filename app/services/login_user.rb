# frozen_string_literal: true

class LoginUser
  def initialize(email:, password:)
    @user = User.find_by(email: email)
    @password = password
  end

  def call
    return ErrorResponse.new([I18n.t('errors.not_found', resource: 'User')], 404) if @user.nil? || ActsAsTenant.current_tenant.nil?

    if user.authenticate(password) && authorized_tenant?
      SuccessResponse.new({ auth_token: Knock::AuthToken.new(payload: @user.to_token_payload), user: @user })
    else
      ErrorResponse.new([I18n.t('authentication_failed')], 401)
    end
  end

  private

  attr_reader :user, :password

  def authorized_tenant?
    return true unless user.course_creator?

    user.account_identifier == ActsAsTenant.current_tenant.account_identifier
  end
end
