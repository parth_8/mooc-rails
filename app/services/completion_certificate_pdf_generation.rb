# frozen_string_literal: true

class CompletionCertificatePdfGeneration
  def initialize(completion_certificate)
    @completion_certificate = completion_certificate
    @student = completion_certificate.student
    @user_course = completion_certificate.user_course
  end

  def call
    pdf_string = render_wickedpdf
    return ErrorResponse.new([@error_message]) if @error_message.present?

    save_path = generate_tempfile(pdf_string)
    if @error_message.present?
      ErrorResponse.new([@error_message])
    else
      SuccessResponse.new(save_path.to_s)
    end
  end

  private

  attr_reader :completion_certificate, :student, :user_course

  def render_wickedpdf
    pdf_string = WickedPdf.new.pdf_from_string(
      ActionController::Base.new.render_to_string(
        template: 'completion_certificates/certificate.pdf.erb',
        locals: locals_information,
        layout: false
      ),
      pdf: "certificate_#{completion_certificate.id}",
      layout: false,
      page_size: 'A4',
      lowquality: false,
      handlers: [:erb],
      formats: [:html],
      disable_smart_shrinking: true,
      margin: { top: 136 },
      orientation: 'Portrait',
      disposition: 'attachment'
    )
    pdf_string
  rescue StandardError => e
    Rails.logger.error("[CompletionCertificatePdfGeneration][render_wickedpdf] #{e.message}")
    @error_message = e.message
    nil
  end

  def locals_information
    {
      :@student => student,
      :@course => user_course.course,
      :@user_course => user_course
    }
  end

  def generate_tempfile(pdf_string)
    return nil if pdf_string.nil?

    FileUtils.mkdir_p('tmp/pdfs/certificates')
    save_path = Rails.root.join('tmp/pdfs/certificates', "certificate_#{completion_certificate.id}.pdf")
    @file_data = File.open(save_path, 'wb') do |file|
      file << pdf_string
    end
    save_path
  rescue StandardError => e
    Rails.logger.error("[CompletionCertificatePdfGeneration][generate_tempfile][pdf_string #{pdf_string}] #{e.message}")
    @error_message = e.message
    nil
  end
end
