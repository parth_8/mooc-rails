# frozen_string_literal: true

class SuccessResponse
  attr_reader :data
  def initialize(data)
    @data = data
  end

  def success?
    true
  end
end
