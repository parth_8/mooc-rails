# frozen_string_literal: true

class TrackCourseProgressWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(user_assignment_id)
    user_assignment = UsersAssignment.find(user_assignment_id)
    user_course = user_assignment.user_course

    assignment_ids = user_course.course.assignment_ids
    user_assignment_ids = UsersAssignment.where(user_id: user_assignment.user_id, assignment_id: assignment_ids).pluck(:assignment_id)
    uniq_assignments_completed = user_assignment_ids.uniq.length

    user_course.assignments_completed = uniq_assignments_completed
    user_course.completed_at = Time.zone.now if user_course.assignments_completed == assignment_ids.length
    user_course.save!
  rescue StandardError => err
    logger.error("[TrackCourseProgressWorker#perform] error: #{err.message}")
  end
end
