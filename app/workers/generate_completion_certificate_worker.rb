# frozen_string_literal: true

class GenerateCompletionCertificateWorker
  include Sidekiq::Worker

  def perform(user_course_id)
    user_course = UserCourse.find(user_course_id)
    completion_certificate = user_course.create_completion_certificate!(student_id: user_course.user_id)
    resp = CompletionCertificatePdfGeneration.new(completion_certificate).call

    if resp.success?
      save_document(completion_certificate, resp.data)
    else
      logger.error("[GenerateCompletionCertificateWorker] error: #{resp.errors.inspect}")
      false
    end
  end

  def save_document(completion_certificate, file_path)
    completion_certificate.update!(document: File.open(file_path, 'rb'))
  rescue StandardError => e
    logger.error("[GenerateCompletionCertificateWorker#save_document(certificate##{completion_certificate}, #{file_path})] error: #{e.message}")
    nil
  ensure
    File.delete(file_path) if File.exist?(file_path)
  end
end
