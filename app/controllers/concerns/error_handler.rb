# frozen_string_literal: true

module ErrorHandler
  extend ActiveSupport::Concern

  included do
    rescue_from StandardError, with: :standard_error_handler
    rescue_from ActionController::ParameterMissing, with: :param_missing_error_handler
    rescue_from ActiveRecord::RecordNotFound, with: :record_not_found_handler
    rescue_from ActiveRecord::RecordInvalid, with: :invalid_record_handler
    rescue_from Auth::InvalidTokenError, with: :unauthenticated_access_handler
    rescue_from Auth::UnauthorizedAccessError, with: :unauthorized_entity
  end

  def invalid_record_handler(err)
    error_response(422, I18n.t('validation_failed'), err.record.errors.full_messages)
  end

  def record_not_found_handler(err)
    Rails.logger.error("Records Not Found for Type: #{err.model} ID: #{err.id}")
    error_response(404, I18n.t('errors.not_found', resource: err.model))
  end

  def param_missing_error_handler(err)
    error_response(400, I18n.t('bad_request', param_name: err.param), [])
  end

  def standard_error_handler(err)
    log_error(err) if Rails.env.development?
    render json: {
      status: 500,
      message: err.message,
      errors: err.backtrace
    }
  end

  def unauthenticated_access_handler
    error_response(401, I18n.t('unauthenticated_access'))
  end

  def unauthorized_entity(_err)
    error_response(403, I18n.t('unauthorized'))
  end

  def log_error(err)
    puts '-----------------------------------------------------------------'
    puts err.message
    puts 'BACKTRACE'
    puts err.backtrace
    puts '-----------------------------------------------------------------'
  end

  def error_response(error_code = nil, message = nil, errors = [])
    render json: {
      status: error_code || 500,
      message: message || I18n.t('internal_error'),
      errors: errors || []
    }
  end
end
