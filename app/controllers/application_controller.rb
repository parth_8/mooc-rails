# frozen_string_literal: true

class ApplicationController < ActionController::API
  set_current_tenant_by_subdomain(:user, :account_identifier)
end
