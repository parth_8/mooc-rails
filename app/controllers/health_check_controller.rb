# frozen_string_literal: true

class HealthCheckController < ApplicationController
  def index
    render json: { success: true }, status: :ok
  end
end