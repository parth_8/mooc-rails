# frozen_string_literal: true

class Section < ApplicationRecord
  acts_as_sequenced scope: :course_id
  enum duration_type: %i[weekly monthly]

  belongs_to :course
  has_many :lessons, dependent: :destroy
  has_one :assignment, dependent: :destroy

  validates :title, :duration_type, presence: true

  scope :editable_by_user_id, lambda { |user_id|
    joins(:course).where(courses: { user_id: user_id })
  }

  def self.by_role(user_id, role)
    if role == 'course_creator'
      joins(:course).where(courses: { user_id: user_id })
    else
      all
    end
  end
end
