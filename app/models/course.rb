# frozen_string_literal: true

class Course < ApplicationRecord
  acts_as_tenant(:user)
  # attributes
  enum status: { drafted: 0, published: 1, archived: 3 }
  searchable_attributes :title, :description

  # associations
  belongs_to :created_by, class_name: 'User'
  has_many :course_tags, dependent: :destroy
  has_many :tags, through: :course_tags
  has_many :sections, dependent: :destroy
  has_many :lessons, dependent: :destroy
  has_many :assignments, through: :sections
  has_many :user_courses, dependent: :destroy
  has_many :students, through: :user_courses, source: :user

  # validations
  validates :title, :description, :price, :status, presence: true
  validates :title, length: { in: 1..120 }
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :entrollment_allowed_till, presence: true, if: :published?
  validate :entrollment_allowed_till_value, if: :entrollment_allowed_till_assigned?

  # class methods
  scope :by_tag_names, ->(tag_names) { joins(:tags).where(tags: { name: tag_names }) }
  scope :open_for_enrollment, -> { published.where('entrollment_allowed_till >= ?', Time.zone.now) }
  def self.filter_by_status(input_status)
    case input_status
    when 'all'
      all
    when 'drafted', 'published', 'archived'
      public_send(input_status)
    else
      none
    end
  end

  def self.filter_by_price(min, max)
    if max.blank? || min.blank? || max < min
      none
    else
      where('price between :min and :max', min: min, max: max)
    end
  end

  def self.by_role(current_user_id, role)
    if role == 'course_creator'
      where(user_id: current_user_id)
    else
      all
    end
  end

  def entrollment_allowed_till_assigned?
    will_save_change_to_attribute?(:entrollment_allowed_till) && !entrollment_allowed_till.blank?
  end

  def entrollment_allowed_till_value
    return true if entrollment_allowed_till > Time.zone.now

    errors.add(:entrollment_allowed_till, :invalid)
  end
end
