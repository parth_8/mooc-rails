# frozen_string_literal: true

class Lesson < ApplicationRecord
  acts_as_sequenced scope: :section_id

  belongs_to :course, counter_cache: true
  belongs_to :section, counter_cache: true
  has_attached_file :media,
                    default_url: '',
                    less_than: 30.megabytes

  validates_attachment_content_type :media, content_type: %w[application/pdf text/plain]
  validates :title, presence: true, length: { in: 1..120 }
  validates :section_id, :course_id, presence: true

  scope :editable_by_user_id, lambda { |user_id|
    joins(:course).where(courses: { user_id: user_id })
  }

  before_validation :assign_course_id, if: :section_updated?

  def section_updated?
    new_record? || will_save_change_to_attribute?(:section_id)
  end

  def assign_course_id
    self.course_id = section&.course_id
  end
end
