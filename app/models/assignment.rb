# frozen_string_literal: true

class Assignment < ApplicationRecord
  acts_as_tenant(:user)
  belongs_to :section

  has_attached_file :content_media, default_url: '', less_than: 30.megabytes
  validates_attachment_content_type :content_media, content_type: %w[application/pdf text/plain]

  validates :title, presence: true, length: { in: 1..120 }
  validates :section_id, presence: true, uniqueness: true
  validate :current_tenant_for_section

  def current_tenant_for_section
    return true if ActsAsTenant.current_tenant.present? && Section.where(course_id: Course.where(user_id: ActsAsTenant.current_tenant&.id)).exists?

    errors.add(:section_id, :invalid)
  end
end
