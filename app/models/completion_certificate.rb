# frozen_string_literal: true

class CompletionCertificate < ApplicationRecord
  acts_as_tenant(:user)
  belongs_to :user_course
  belongs_to :student, class_name: 'User'

  has_attached_file :document, default_url: '', less_than: 5.megabytes
  validates_attachment_content_type :document, content_type: ['application/pdf']

  validates :reference_number, :user_id, presence: true
  validates :reference_number, uniqueness: true

  before_validation :generate_reference_number

  def generate_reference_number
    self.reference_number = "MOOC#{SecureRandom.urlsafe_base64(8).upcase}" if reference_number.blank?
  end
end
