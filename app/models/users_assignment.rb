# frozen_string_literal: true

class UsersAssignment < ApplicationRecord
  belongs_to :user
  belongs_to :assignment
  belongs_to :user_course

  has_attached_file :submission, default_url: '', less_than: 30.megabytes
  validates_attachment_content_type :submission, content_type: %w[application/pdf text/plain]

  after_create_commit :track_course_progress
  def track_course_progress
    TrackCourseProgressWorker.perform_async(id)
  end
end
