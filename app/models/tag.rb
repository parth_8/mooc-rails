# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :course_tags, dependent: :destroy
  has_many :courses, through: :course_tags

  validates :name, presence: true, length: { in: 1..30 }, uniqueness: true

  before_validation :sanitize_name
  def sanitize_name
    self.name = name&.downcase&.strip
  end

  def self.search(term)
    return all if term.blank?

    where('name ilike ?', "%#{term.downcase}%")
  end
end
