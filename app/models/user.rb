# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password
  enum role: { student: 0, course_creator: 1 }

  has_many :courses, dependent: :destroy
  has_many :user_courses, dependent: :destroy
  has_many :enrolled_courses, through: :user_courses, source: :course
  has_many :user_assignments, dependent: :destroy
  has_many :completion_certificates, dependent: :destroy

  # validations
  validates :email, presence: true, uniqueness: true, format: { with: /\A[^@\s]+@[^@\s]+\z/ }
  validates :account_identifier,
            presence: true,
            uniqueness: true,
            format: { with: /\A[a-zA-Z]+\z/, message: 'only allows letters' },
            length: { in: 3..6 },
            if: :course_creator?
  validates :password, :role, presence: true
  validates :role, inclusion: { in: %w[student course_creator] }

  # scopes
  scope :by_course_ids, ->(course_ids) { joins(:user_courses).where(user_courses: { course_id: course_ids }) }

  # callbacks
  before_validation :sanitize_email
  before_validation :sanitize_account_identifier

  def sanitize_email
    self.email = email&.downcase&.strip
  end

  def sanitize_account_identifier
    self.account_identifier = account_identifier&.downcase&.strip
  end

  # knock
  def to_token_payload
    { id: id, issued_at: DateTime.current }
  end

  def self.from_token_payload(payload)
    find(payload['id'])
  end
end
