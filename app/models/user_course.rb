# frozen_string_literal: true

class UserCourse < ApplicationRecord
  enum status: { in_progress: 0, completed: 1 }
  belongs_to :course
  belongs_to :user
  has_one :completion_certificate, dependent: :destroy

  validates :user_id, uniqueness: { scope: [:course_id] }
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  before_save :mark_as_completed, if: :completed_at_assigned?
  after_commit :generate_completion_certificate, if: :completed_at_assigned_after_commit?

  def completed_at_assigned?
    will_save_change_to_attribute?(:completed_at) && !completed_at.blank?
  end

  def completed_at_assigned_after_commit?
    saved_change_to_attribute?(:completed_at) && !completed_at.blank?
  end

  def mark_as_completed
    self.status = :completed
  end

  def generate_completion_certificate
    GenerateCompletionCertificateWorker.perform_async(id)
  end
end
